#include<iostream>
#include<cstdint>
#include<cmath>
#include<iomanip>

typedef int16_t fixed_point_t;
typedef int32_t fixed_point_2x_t;

#define N 8 // Fixed point resolution

fixed_point_t floatToFp(float x){
    fixed_point_t sign = x < 0.0 ? -1 : 1;
    return  sign * std::round( fabs(x) * std::pow(2,N) );
}

float fpToFloat(fixed_point_t x){ 
    return ( (float) x ) / std::pow(2, N);
}

fixed_point_t fpAdd(fixed_point_t a, fixed_point_t b)
{
    return a + b;
}

fixed_point_t fpSub(fixed_point_t a, fixed_point_t b)
{
    return a - b;
}

fixed_point_t fpMlt(fixed_point_t a, fixed_point_t b)
{
    fixed_point_2x_t temp = (fixed_point_2x_t) a * b;
    
    // add 1/2 to give correct rounding
    temp = temp + (1 << N-1); 
    
    // divide by 2^N
    temp = temp >> N;   
    
    // saturate the result before assignment
    if( temp > INT16_MAX){
        return INT16_MAX;
    }
    if( temp < INT16_MIN){
        return INT16_MIN;
    }

    return temp;
}

fixed_point_t fpDiv(fixed_point_t a, fixed_point_t b)
{
    fixed_point_2x_t temp = a << N;
    
    // add b/2 to give correct rounding
    temp += b >> 1;
    
    // perform the division
    temp = temp / b;

    // truncate and return
    return temp;
}

int main(){

    float a = 0, b = 0;
    fixed_point_2x_t a_fp, b_fp;

    std::cout << "Give me a: " << std::endl;
    std::cin >> a;

    while( b == 0 ){
        std::cout << "Give me b (b != 0): " << std::endl;
        std::cin >> b; 
    }

    a_fp = floatToFp(a);
    b_fp = floatToFp(b);

    std::cout << "a: " << a_fp << "\tb" << b_fp << std::endl;
    std::cout << std::left << std::setw(20) << "Operation" 
        << std::setw(20) << "Fixed p. result" 
        <<  std::setw(20) << "To decimal" 
        <<  std::setw(20) << "Float result" << std::endl;
    std::cout << std::right << std::setw(4) << a << " + " << std::left << std::setw(13) << b
        << std::setw(20) <<  fpAdd(a_fp,b_fp) 
        <<  std::setw(20) <<  std::setw(20) 
        << fpToFloat(fpAdd(a_fp,b_fp)) 
        <<  std::setw(20) << a + b << std::endl;
    std::cout << std::right << std::setw(4) << a << " - " << std::left << std::setw(13) << b 
        << std::setw(20) <<  fpSub(a_fp,b_fp) 
        <<  std::setw(20) <<  std::setw(20) 
        << fpToFloat(fpSub(a_fp,b_fp) ) 
        <<  std::setw(20) << a - b << std::endl;
    std::cout << std::right << std::setw(4) << a << " * " << std::left << std::setw(13) << b
        << std::setw(20) <<  fpMlt(a_fp,b_fp) 
        <<  std::setw(20) <<  std::setw(20) 
        << fpToFloat(fpMlt(a_fp,b_fp) ) 
        <<  std::setw(20) << a * b << std::endl;
    std::cout << std::right << std::setw(4) << a << " / " << std::left << std::setw(13) << b
        << std::setw(20) <<  fpDiv(a_fp,b_fp) 
        <<  std::setw(20) <<  std::setw(20) 
        << fpToFloat(fpDiv(a_fp,b_fp)) 
        <<  std::setw(20) << a / b << std::endl;
    
    return 0;
}

