GXX             = g++
GCC             = gcc
LD              = g++

FLAGS           = -std=c++11

SRCS = main.cpp
OBJS=$(SRCS:.cpp=.o)

# main
main: $(OBJS)
	$(LIBTOOL) g++ -o $@ $(OBJS) $(LDFLAGS)

.cpp.o:
	$(LIBTOOL) --mode=compile g++ -c $(FLAGS) $<

clean:
	rm -f main *.o *.lo
	rm -rf .libs
