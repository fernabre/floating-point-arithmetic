# Fixed point arithmetic

Basic implementation of fixed point arithmetic functions in C/C++.

These functions are useful to operate over decimal values in processors that lack a floating point unit.